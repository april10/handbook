# Justin

## Hello World! 

My name is *Justin* and I'm pumped to be a part of the DoubleGDP team. 

**About me.** Born and raised in a small town in NJ right outside NYC. Have an aspiration to live in East Africa, but have been living with my fiancee in Hong Kong for the past 2 years. 

### Things I like
* The Yankees 
* Reading (non-fiction mostly!)


### Things I don't like
- The Boston Red Sox and possibly an not-to-be-named team from Central Texas

[This is me testing out a link](https://www.doublegdp.com/about/)
