Hello World from Cecilia

My name is Cecilia Eis and I was born and raised in Buenos Aires, Argentina. I lived in Florida, US for 3 years when I was younger and it's probably the reason why I LOVE the beach so much. 

Here's a few things about me:

* I have 4 daughters, and YES, it's a lot of girls in one house.

* I love to travel, learn about different cultures, explore the world, spend time with my family on each trip and make unforgettable memories for them.

* Still deciding if I prefer to watch soccer or Sunday's barbecue, or maybe both during the Soccer World Cup.

* I enjoy cooking and trying new recipes and ingredients.

* Yoga keeps me sane!
