Hello World,

My name is April, and I love laughing. In high school, I was the introverted class clown cracking jokes at the back of the classroom. Those around me would laugh and get in trouble, but I was never reprimanded because of my quiet and shy demeanor.

Some things I enjoy:

* A clean house - I tend to go above and beyond with cleaning (some call it OCD).

* Video games - Retro, PC, console, you name it!

* Yardwork - Excludes raking leaves

* Biking & kayaking - Leisure pace of course

* Nature - I have to live near trees or water to be happy

* Family - I am the fifth of eight children and love hanging out at large family gatherings

* Dark crime docuseries or sad reality shows - Long Lost Family, Unsolved Mysteries, Making a Murderer, The Mind of Aaron Hernandez, Evil Genius...

* Reading mental health/medical articles - I know too many trauma victims, many of which are close to me. I read articles to understand their communication style and needs.

* Going to the movies.

