---
title: Local Handbook Repo
---

## Set up a local copy
You can contribute entirely through the online process above. However, you may also wish to set up a local copy of the handbook. This will make it easier to preview your changes and also gives you access to a few other writing tools that make it more user friendly. There's a bit more complexity to this process, but this guide should get you going.

To set up a local version of this handbook, you will need at least the following set up on your machine:

1. Git -- see these [installation instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
1. A Markdown editor -- we recommend [Typora](https://typora.io/) for a simple visual experience or [Atom](https://atom.io/) if you want an environment integrated with git.
1. A local copy of the repository (instructions below)

This setup will be sufficient to make changes to the documentation, and is user-friendly for non-developers. You'll be able to see and contribute the files within the documentation, but won't have the same UI as on the website. If you want that full experience, you please install [MkDocs](https://www.mkdocs.org/).

![GitLab Notification Settings](../img/handbook-notifications.png)

### Setting up Git
You will need to set your credentials in git. This may be a bit daunting at first, so we'll step through it together. Instructions are [here](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)

<details>
<summary>Git setup for Mac</summary>

<p>From your terminal, run the following:</p>
<ol>
<li> <code>git config --global user.name "John Doe"</code></li>
<li> <code> git config --global user.email "johndoe@doublegdp.com"</code></li>
</ol>
</details>



### Local copy of repo
To create a remote copy of the repository, open your terminal interface and navigate do the directory where you want to store it. (Nolan suggests `~/Projects`.) Then run `git clone https://gitlab.com/doublegdp/handbook.git`; this will create a `handbook` directory that has the latest copy of the project.



## Make your first edit
To make your first edit:

1. In your terminal, run `git pull`
1. Open your markdown editor from the `docs/` directory within the Handbook
1. In your markdown editor, make a change and save it
1. In your terminal run `git add .` or `git add docs/name_of_changed_file.md `
1. In your terminal, run `git commit -m "Make first contribution"`[^commit]
1. In your terminal, run `git push`



[^commit]: This creates a 'commit' with a message. A `commit` is an individual change to a set of files. Your message should be a brief and concise description of a simple change you're making to the docs. Make the first word of your commit message a verb, and use present tense.
