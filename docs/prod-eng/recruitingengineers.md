# Guidelines for Interviewing Engineers

I am a DoubleGDP Engineer and I am interviewing an engineer like me, What do I do?

- **Come prepared**

  - Read the candidate 's resume with care and note the areas you like and the areas where you want clarification.

  - Depending on the tech stack, **review the questions list** for the appropriate stack to get you in thinking about insightful questions you want to ask the candidate.   The questions list are just a guide to help you.  You are not required to used it (although everyone does.)

    - General ReactJS. Questions list that we can ask engineers during an interview can be found [here](https://docs.google.com/document/d/19qaK11bz2hdTbHIdDLFPkjN8XmlAvi6J2CmxfQGEdiY/edit#heading=h.ybbld1bahdwl) --  Probably shamelessly compiled from other web sites.

    - General Rails (*Coming Soon*)

  - If the candidate has been given a **Coding Challenge**, you must

    - **Complete the coding Challenge** yourself and that you understand it completely.   
    - Install the candidate's coding challenge on your environment, make sure **the project runs** and **study the code** so that you can ask questions.  
      - How was the install process documented?
      - Was the code clean?  Did the code went through a Linter?
      - Was the code readable ?
      - Was there Unit tests?   What about Code coverage?
      - Compare your findings with the seniority of the position. 

  - You can find the coding challenges here:

    - [For Rails](https://docs.google.com/document/d/1qJY10sceHmqtKHs5Coc6apDurWUXaTsV8m7A0j9BtOU)
    - [For ReactJS](https://docs.google.com/document/d/1WtqgYuKnYAM8qVeK-DOe_LXvOAzm6zD5Hm0YbP8kTok)

- After The Interview

  - Go to Greenhouse.io 
  - Fill out the evaluation form.
  - Discuss your observations with the hiring manager , but no one else until everyone in the interviewer group have completed their evaluations in greehouse.