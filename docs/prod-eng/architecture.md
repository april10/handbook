# Architecture Design
At DoubleGDP, we use ADR to communicate Architecture Concepts and Decisions.   Not everything is an ADR and they usually contains changing design concepts that will affect the team .
https://resources.sei.cmu.edu/asset_files/Presentation/2017_017_001_497746.pdf

## ADR
Template for ADR can be found at:
https://gitlab.com/doublegdp/app/-/wikis/Architectural-Decision-Records
ADR are currently kept in the gitlab wiki. 
To submit an ADR: 
 - Create a new wiki page and paste the Markdown from the link above
 - Fill in the requested information and sent an email / slack to the team for review / comments / discussions/ brainstorming.
