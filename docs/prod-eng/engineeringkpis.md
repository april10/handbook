# KPIs for Engineering

We are firm beleivers of measuring successes and failures.   And most times, what gets measured and communicated will improve over time.   

At DoubleGDP, we measure a few data points over time to make sure we can consistently improve  and spot trouble some trends before becoming unruly.  Some data points are measured constantly, and a overall data summary is compiled at the end of every week.

The KPIs are reported on the [Engineering Dashboard](https://drive.google.com/open?id=1CNo6hSDUlxQ_ATelKq1N9S6A_bvVbCBW) 



### Production Performance KPIs (Measured Weekly)

- Rollbar(What we use for our Errors and log aggregator) Issues Count - Lower is better

- Production Issue (What is reported by users in production) Count - Lower is better



### Sprint Performance KPIs (Measured Weekly)

- Rails code Coverage (Weekly)




- Blended Coverage (Weekly)

- Merge Request Count  (Weekly)
- How many times we pushed to Production (Weekly)

- Number of Stories in Sprint  (set every 2 weeks)

- Number of Stories completed during the week

- Sprint Success Ratio - set every 2 weeks
  - Number of Stories completed during the sprint/Number of Stories in Sprint
  - Exclude Production issue

- Bugs Count - found during the week (Weekly)

  