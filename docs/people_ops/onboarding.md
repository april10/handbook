

## Onboarding process

Welcome to the team!

Before you join, your manager should create an onboarding ticket in GitLab. To do this:

1. Go to [https://gitlab.com/doublegdp/handbook/-/issues/new](https://gitlab.com/doublegdp/handbook/-/issues/new)
1. Create a Title of "Onboarding -- <employee_name>"
1. Select `Choose a template` next to "Description"
1. Select the `New Teammate Onobarding` template. This template has todos for the new teammate, for their manager, and to help set up the proper administrative needs.
1. Schedule a daily check-in for each day of the first two weeks. Add [this document](https://docs.google.com/document/d/1bwpASnXhPPOZSlwsA4zxVMq_JLe4I7kMZZWmAoVHuHw/edit#heading=h.3yw2kts4rqy5) as the notes for the agenda

### Schedule
Your first week will be "company onboarding", led by Nolan, and your second week will be "role onboarding," led by your manager. 

In week 1, we will cover these topics:

1. [Company Overview](../company/index.md)
1. [Values](../company/index.md#values)
1. [Communications](communications.md)
1. [Meeting norms](../company/meetings.md)
1. [Handbook-first process](../handbook/index.md) and training on its first use cases
1. Read the [overview of our customers](https://docs.google.com/document/d/1NFHF1v3iHJyG8KHVvOEbjFGihQmYx0dnHmk0ceMCaCI/edit#heading=h.5mzg90e0f5op) and share an observation with your onboarding team
1. What's novel / uncommon about DGDP practices
1. Go through expectations of your [workstation setup](workstation_setup.md)

In week 2, you will work with your manager to:

1. Learn about our [customer council process](../company/customer_council.md) and review the latest slides from that page -- this will give you an update on our last strategic input from them
1. Review our [company goals](../company/goals.md) and draft personal goals for yourself for 6 weeks, 3 months, and 6 months if it's helpful
1. Define your personal goals 
1. Understand your role and responsibilities


### Administrative Needs
With each teammate we welcome, we learn a bit more about how to bring people onboard. As we learn, we update the template that we use, which is located in the Handbook source code at `.gitlab/issue_templates/New Teammate Onboarding.md`

