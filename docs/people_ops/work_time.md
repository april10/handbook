---
title: Work Time and Time Off Policy
---
## Work Availability and Paid Time Off

We emulate GitLab’s “don’t ask, must tell” policy, and encourage people to take the time off. It's up to each teammate to choose what days will work best for them, but we generally encourage you to take your national Holidays, in addition to some long weekends and vacations.

References that may be helpful:

* [GitLab's Paid Time Off Policy](https://about.gitlab.com/handbook/paid-time-off/)
* [GitLab's Guidance on Mental Health](https://about.gitlab.com/company/culture/all-remote/mental-health/)

### How to Take Time Off

You can take time off whenever you want. However, please be responsible in when you take it and how you communicate it. Here are a few things to consider:

1. Check with your manager to try to avoid being out around a critical milestone or at the same time that others on your team will be out
1. Try to provide advance notice -- a general guideline would be to share twice as many days in advance as you'd like to take off. e.g. two days' notice is for one day off; two weeks in advance would be better if you want to take a full week.
    * Note that this does not apply for emergencies, and is not a requirement. Sometimes fun things come up last minute that you want to do. But this guideline will help prevent leaving others in the lurch when you have some flexibility in scheduling.
1. Team members who are with us through an agency should also follow that agency's procedures in addition to ours.
1. If you will miss the sprint video update, please record one in advance so that others will be aware of what you contribution during the sprint

Once you've decided to take time off and your manager is aware, it should be communicated to the rest of the team. Here's how:

1. Add your time off to your calendar, with the hours of the day blocked off so that meetings won't be scheduled
1. Email [all@doublegdp.com](mailto:all@doublegdp.com) with the dates you will be away (this is not necessary for National holidays planned in advance; see below for that process)
1. Add an out-of-office response in your email settings with the start and end date of your time off so that people reaching out to you on email will be notified

### How to Add Bulk Time Off

We encourage you to take the national holidays in your home country.  We've made this easy by providing a calendar with those dates so that you can add them in bulk.

1. Visit our [Important Dates Spreadsheet](https://docs.google.com/spreadsheets/d/1POhDzesk5fEjiafkWxJmsFnjbY5F_c--0fmFdSiO2vE/edit#gid=882612580) and see the tab for your National Holidays
1. Create a local CSV file of that tab, using the menu option `File: Download: CSV`.
    * After you create a copy, feel free to add or remove dates as fit to your circumstances
    * Note that holidays that occur on weekdays have start and end times, so that importing them will block off your calendar
    * [More instructions](https://support.google.com/calendar/answer/37118?co=GENIE.Platform%3DDesktop&hl=en) are available from Google
1. Import into your Google Calendar using its `Settings: Import & export` feature
1. Confirm it added the dates properly

Note that you should notify your manager when you opt which dates to take and if you made an deviations from these defaults. It's not necessary to email all@ if you use these default dates.

## Time Zones

We aim that most of our work should by "asynchronous" -- meaning that you can do it on your own schedule and not be dependent on meeting with another teammate in order to tackle it. However, coordination is also important both for work and social reasons. Since we span time zones, currently `US/Pacific` and `Africa/Lusaka`, we ask that team mates make themselves generally available on Slack and for meetings during those hours that are the best overlap between them:

| Overlap | US/Pacific | Africa/Lusaka |
| --- | --- | --- |
| Start Time | 7:00 am | 16:00 |
| End Time | 10:30 am | 19:30 |

*Note that these are overlap times while the US observes Daylight Saving Time. We will adjust in November when this period is over.*
