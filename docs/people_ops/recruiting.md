# Recruiting

We aim to build a world-class team that in 2020 will look like this:



*   1 CEO / Head of Product
*   1 Head of Engineering
*   3-4 Engineers (back-end and front-end)
*   1 Designer
*   1-2 Customer Success Managers
*   1 Head of Partnerships & Operations


## Jobs and boards

We use [Greenhouse](https://app3.greenhouse.io/) to host all jobs and manage our recruiting and interview process. All team members are welcome to participate, and will be asked to play different roles for different positions. Our open jobs are listed on [our website](https://www.doublegdp.com/about/).


## Factors Looking For

At a high level, we value teammates with intelligence, integrity, and strong work ethic. These traits should be looked for during the interview process. In addition, there are specific factors relevant to all candidates applying to DoubleGDP:



1. **Alignment** - clear what candidate is looking for and that it aligns with what we need
2. **Domain experience** - SaaS software, new product launches, startup companies. Note: focus on stronger of resume / LI Profile, recognizing that different people put emphasis on different mediums.
3. **Tech expertise** - web technologies, agile stack
4. **Leadership** - have a player / coach mentality. Want to contribute by doing and by helping others.
5. **Communication** - concise, clear, and able to cite specifics. They write and communicate well, and avoid glaring errors.
    1. Note that we should make affordances for challenges commonly faced by non-native speakers. For example, issues that spell check can catch should be a red flag, but more subtle grammatical or phrasing that may seem awkward to a native speaker should not.

Each job may have other specific requirements that will be kept in the job description or scorecards within Greenhouse.

## Engineering interview process
Open positions are posted on our [job board](https://doublegdp.com/jobs). Please apply through there, and your application will be reviewed and responded to. If it appears there's a good potential fit, here's an overview of the steps in our engineering interview process:

1. Initial introduction - 25 minutes with either CEO or Head of Engineering. What we'll go through:
     * High-level meeting call to ensure the candidate and DoubleGDP align on job requirements
     * What DoubleGDP and the position are about to ensure it matches your expectations
     * Resume walk through – We want to make sure we fully understand your work experience
     * What you are looking for in your ideal role?
     * What you should expect in the next steps of the hiring process
1. Coding challenge - 2-3 hour exercise to demonstrate your strengths
1. Technical interview - 50 minutes with Head of Engineering or appropriate technical resource
1. [CEO interview](#ceo-interview) - 50 minutes with CEO
1. Interview panel - 40 minutes with a representative of each team (engineering, partners, customer success)
1. References -- 3-5 references including a manager, a peer, and cross-functional partner

## UI/UX Designer interview process
Open positions are posted on our [job board](https://doublegdp.com/jobs). Please apply through there, and your application will be reviewed and responded to. If it appears there's a good potential fit, here's an overview of the steps in our engineering interview process:

1. Initial introduction - 25 minutes with either the Product Manager or CEO. What we'll go through:
     * High-level meeting call to ensure the candidate and DoubleGDP align on job requirements
     * What DoubleGDP and the position are about to ensure it matches your expectations
     * Resume walk through – We want to make sure we fully understand your work experience
     * What you are looking for in your ideal role?
     * What you should expect in the next steps of the hiring process
1. Competency based interview - 50 minutes with Product Manager
1. [CEO interview](#ceo-interview) - 50 minutes with CEO
1. Interview panel - 40 minutes with a representative of each team (engineering, partners, customer success)
1. References -- 3-5 references including a manager, a peer, and cross-functional partner

## Interview Questions

### CEO Interview

Before your CEO interview, please be sure to have watched our most recent [sprint update](https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig/playlists?view_as=subscriber).

1. How did you first become interested in the field of work? How have your interests changed over time, and what has influenced your choices?
1. What interested you in DoubleGDP? Have you been looking at other opportunities? If so, what do they have in common, and in what ways are they different?
1. What aspects of our mission excite you most? Are there areas that you see as potential concerns? What do you think will be some challenges we will face in the next 6-12 months and what are your thoughts on how to address them? (Please share some thoughts about challenges pertinent both to our company as a whole and to your role in particular.)
1. Tell me about your experience with remote work. What are some of the practices you've developed to be effective? What practices from the company have you experienced that were helpful or detrimental?
1. Tell me about a colleague with whom you've worked in the past who you thought was truly exceptional. What qualities or practices did they demonstrate? In what ways are you similar to them, and in what ways are you different?
1. Please consider the best and next best teams you've worked with. How would you characterize them? What did they have in common, and what distinguished the best from its next counterpart?
1. What did you observe in our last sprint update?
1. What questions do you have for me?

Note that this is not necessarily exhaustive. We may also explore specific questions that arose from my review of your experience, your fit with DoubleGDP, or discussions from previous rounds of interviews.



## Candidate Frequently Asked Questions

Here are frequently asked questions and our answers:



1. What’s is your funding situation?
    1. We’re funded by a single investor through mid 2021 and not expecting revenue within this timeframe. Our goal is to demonstrate product market fit through WAU growth and operational customers, and raise in that timeframe.
    1. Our investor is Sid Sijbrandij, the founder and CEO of Gitlab. He has built a successful remote-only company that provides an end-to-end platform in a market full of fragmented solutions. In addition to funding, he provides advice and relationships to help us learn from and build upon Gitlab’s success.
1. What’s your stance on remote work?
    1. We’re committed to being an all-remote company with no offices. We think this yields better results: it will build a culturally diverse workforce necessary to operate in cities around the world, offers great advantages to employees like control over personal time and no commute, and is much more productive than semi-remote environments that disadvantage remote employees over those at headquarters.
    1. This does take an investment in the process to make work, for example: good hygiene around documented communications, a handbook that’s operationally useful and gets contribution from the whole company, and team social chats -- agendaless meetings just for socializing. These are good practices anyway, but many companies don’t invest sufficiently because so much of their process evolves informally.
1. How big are you?
    1. Our current team is listed on our website: https://www.doublegdp.com/about/
1. What’s your value proposition?
    1. For cities, we help them attract and build relationships with residents, deliver and improve municipal services, and have an innovative and cost-efficient software infrastructure.
    1. For residents, we provide access to all city services in one place and a way to give feedback and be heard. We expect this to build strong communities and better places to live.
1. What’s the compensation range?
    1. We aim to pay at the 50th percentile of the local market in which we're hiring. We factor in the role, seniority, and location. Your exact compensation amount will be based on your experience level and market conditions. Note that we do not offer options/equity at this time. Some references we use to calibrate on market rates are the [GitLab compensation calculator](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/calculator/), [Payscale.com](https://www.payscale.com/), [Glass Door](https://www.glassdoor.com/), [Salary.com](https://www.salary.com/), and [Blind](https://www.teamblind.com/).
1. Why don’t you offer options/equity?
    1. We view this as a huge opportunity, but one that will take a while to mature. We envision a product with a large footprint and know that cities take a while to decide on and adopt new technology. This kind of opportunity better suits a visionary investor than traditional venture capitalist. We’re therefore structured as a sole-proprietorship LLC and our aim is to demonstrate product-market fit and a clear roadmap that warrants larger investment from outside investors. When we achieve this we will reconsider distributing equity to employees.
1. What would be the key responsibilities for the head of customer success in growth?
    1. The full job description is [on our jobs page](https://www.doublegdp.com/jobs/?gh_jid=4081230003). The areas I'd emphasize as key business priorities are: 1) Understand our customer's goals and identify ways they can leverage our platform to achieve them 2) Drive product adoption through customer marketing, training, and understanding user needs 3) Run pilot programs like Artists in Residence that help showcase success 4) Gain insights from customers and users and bring them to product, engineering, and partnerships to help us grow
1. How do you expect the Head of Customer Success to partner with sales?
    1. As Head of CS and an executive at the company, I would expect for you to be actively involved in new sales, and especially in helping them understand the ways that we can partner with them to help accelerate growth.
1. Have you specified growth targets and timelines? How well are you doing against those goals?
    1. Please see details on our [goals page](../company/goals/)
    1. Please see progress against top-line goals from our latest [sprint update](https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig).
1. Is there a defined process that will guide how customers will transition from the current partnership status to revenue generating status?
    1. Our plan is to take a percentage of all transactions through the platform. This aligns our incentives with the city's: to create economic growth. As an end-to-end platform ([see vision here](https://docs.google.com/presentation/d/1c-Ci9QVSc-H0vwtelS0mL4Ka5v2kbzr-1BfOSfLp4d4/edit#slide=id.g5137e6e1a9_0_244)) there are many opportunities to monetize. We haven't yet determined a timeline or process for when we will start monetizing, but plan to approach that when we're delivering indisputable value through the platform.


## References
We request 5 references before making an offer, of which we will speak with at least 3. This helps us learn more about you from those who have worked with you closely. In addition to the obvious benefit of helping us assess what you can bring to the team and gain context on who you are as a person, it also helps set the stage for a productive working relationship with you because we can ask advice on how best to support your success once onboard.

We would like to talk with a current or former colleague from each of these categories. An individual from each of the first three categories is required, and the fourth if you're interviewing for a supervisory role.

1. Your direct (or formerly direct) supervisor - someone responsible for or who oversaw your work
1. A peer in the same role as you - someone who did similar work to you
1. A cross-functional partner or customer - someone who was a consumer of your work
1. Someone you've supervised, if you're applying for a management position
1. Someone else of your choosing

We encourage you to share people who have worked with you closely and know your capabilities well. We also encourage you to ask them to be candid so we can get to understand who you are and what you can bring to the team. We have a [reference script](https://docs.google.com/document/d/1xNIPigVRPgoZZX5fkLvsBY9Dgt77za6pgyIunz33PVs/edit#heading=h.hh2zfqnu4rt) that we use, but do not share in advance because we want candid and spontaneous responses.

## Offer

Offers should be delivered verbally, then with an email, and then with a legal contract. Some team members join us through an agency, in which case the details are worked out in the conversation with their agency. For independent, full-time team members, use this [offer template and script](https://docs.google.com/document/d/1ph51ZB_Cg1p3gkK47I_yBmQ1yEG_cVcwjtHGYdlTC8Y/edit).
