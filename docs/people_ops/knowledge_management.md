# Knowledge Management


## Google Drive



*   By default documents are viewable by everyone at the company. This means anyone with a DGDP email address can see the document if they have the link, but it will not show up in their search results.
*   Generally speaking, when you share a document share it with [all@doublegdp.com](mailto:all@doublegdp.com). We want to allow anyone to contribute to all company work products, and this ensures that when someone needs to access it they can. (There may be some specific cases where information or access rights need to be more restrictive, but please do it sparingly.)
*   Documents should use these prefixes:
    *   [public] - open to everyone, even outside of dgdp
    *   [dgdp] - available to all at dgdp (assumed if not labeled)
    *   [restricted] - shared, but only to specific individuals. May also want to adjust the sharing settings
    *   [private] - might be useful to put this for any files that you wish to keep to yourself.
*   Folders

* To use documents that are stored in a shared drive, it is helpful to create a link to that shared drive from your personal drive. To do this:
    1. Head to [drive.google.com](https://drive.google.com)
    2. Click on “Shared with me”
    3. Tap the name of the folder you want to add to your Google folder
    4. Click on the name of the folder
    5. Click on “Add shortcut to drive”


## Airtable CRM



*   We love sharing knowledge! Please post articles pertinent to our learning about the market to our publicly-available knowledge base here: [https://airtable.com/shrZGPMbZRH4DjTML](https://airtable.com/shrZGPMbZRH4DjTML)
    *   Please share your name in the “Who are you?” dialog, unless anonymity is important to you in sharing.
    *   Ask your friends to share things too, though they don’t need to put their name :)
    *   Sharing here will add it to our repository (viewable on our [website](https://www.doublegdp.com/progress/5_WhatWeAreLearning/)) and create a post to our internal #market-insights channel in Slack
*   We also keep track of companies and people who are relevant to our market learning. That’s kept in airtable directly and not available to the public: [https://airtable.com/tblI8yl7CDFKZc1D2/viwpAFYrRP3u1ze94?blocks=hide](https://airtable.com/tblI8yl7CDFKZc1D2/viwpAFYrRP3u1ze94?blocks=hide)
    *   Note that we’re using the free version of AirTable and limited number of licenses, so we don’t by default make this available to all teammates. But if you’re interested in browsing let us know and we can accommodate.
