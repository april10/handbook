# Security

## In case of lost or stolen property
Employee should immediately notify [all@doublegdp.com](mailto:all@doublegdp.com). The admin should copy [this Asana template task](https://app.asana.com/0/1156946506812257/1170163296177685/f) and assign any subtasks that are appropriate. 
