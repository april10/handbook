# Workstation setup

You need to have a good workstation to work from home to be a productive contributor at DoubleGDP. Full-time teammates may use company money to purchase [office equipment and supplies](https://about.gitlab.com/handbook/spending-company-money/#office-equipment-and-supplies), as they do at GitLab. (Part time teammates or contractors should ask their manager before making purchases.) We mostly follow the guidelines described in their policies, though one important distinction is that within DoubleGDP, purchases exceeding $500 USD are company property and required to be tracked in the [Employee Equipment - Fixed Asset Tracking sheet](https://docs.google.com/spreadsheets/d/1QCesUFmXBiuTcgf2UI5jnc7M4QjqwpcoMhw8rXaGgGs/edit?usp=sharing). There's a link below that provides details on reasonable prices for items on the list, and look at our [finance page](finance.md) for instructions on how to file expenses.

### Required setup

The following setup is required for all teammates:

- Computer -- see [laptop purchase and setup](https://about.gitlab.com/handbook/business-ops/it-ops-team/#laptops) from GitLab
- Camera for videoconferences
- Light that makes your face visible on videoconference calls
- High-speed, reliable internet
- Power source, if it's not reliable in your area
- Quality headset with microphone for videoconferences

GitLab offers guidelines on [reasonable prices](https://about.gitlab.com/handbook/finance/expenses/#hardware) and recommendations on specific pieces of hardware.

### Recommended hardware

These items are highly encouraged:

- External monitor 
- Upgraded webcam -- something better than what comes with your laptop, and that can be mounted on your monitor
- Comfortable, ergonomic chair
- Adjustible height desk, or a [standing desk converter](https://www.amazon.com/VIVO-Adjustable-Converter-Tabletop-DESK-V000V/dp/B0784HWPN6/ref=sr_1_5?dchild=1&keywords=standing+desk+converter&qid=1600277717&sr=8-5)


We recommend you consider these items: 

- Mouse and keyboard
- Coworking space
- Office decorations that help you feel productive or look good on Zoom
- Wifi router
- Android phone
- Internet connectivity

Note that internet connectivity may be expensed if it is used primarily for business and local law permits us to pay for it without incurring taxes. We recommend you aim for at least 10Mbps, but that you make relevant cost / bandwidth tradeoffs based on your local area. Expenses should be submitted monthly using our [expense guidelines](/people_ops/finance/#expense-reports-and-receipts).

### Shipping hardware from the U.S. to Africa

We recommend using the following parcel forwarding services:
- [Heroshe](https://www.heroshe.com/): to ship goods from the U.S. to Nigeria. 
- [iShop Worldwide](https://www.ishop-worldwide.com): to ship goods from U.S. or the U.K. to Zambia or Nigeria.

For both platforms, the process is as followed:

1. Sign up to the platform.
2. Order items to the warehouse address indicated on the respective websites.
3. Pay the shipping fees to platform.
4. Receive items in Nigeria or Zambia.

For warranty reasons, we recommend buying directly from the manufacturer rather than platforms such as Amazon.



