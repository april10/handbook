# Finance

## Invoices

All invoices are required to submit to [accounting@doublegdp.com.](accounting@doublegdp.com)

Vendors are required to submit their invoices within 7 days after the services are rendered or the goods are delivered. Timely submission of your invoices will enable Accounting to record the expenses in the right financial period.

Invoices are usually paid on Net 30 payment terms unless otherwise specified in an agreement.

**US-based Vendors**

For US-based vendors, they are required to provide a completed [W-9 Form](https://www.irs.gov/pub/irs-pdf/fw9.pdf) to [accounting@doublegdp.com](mailto:accounting@doublegdp.com) prior to or at the time when they submit their 1st invoice. 

Approvals of invoices are completed in Bill.com according to the approval hierarchy that's set up by the DGDP's management and Accounting.

Once an invoice is approved, a payment is usually processed in Bill.com according to the payment terms. 

For an efficient payment process, Accounting recommends each vendor to set up Bill.com e-payment. To establish the e-payment, Accounting will send an e-invite to an email that is provided by the vendor. The vendor accepts the invite and provides their bank information within Bill.com. The payment will be scheduled to deposit to the vendor's account according to the payment terms. E-payment will usually arrive the recipient's account within 2-4 business days.
 

**Non-US Vendors**

For non-US vendors and the contracted team members, a completed [W-8BEN Form](https://www.irs.gov/pub/irs-pdf/fw8ben.pdf) is required.

For non-US vendors, vendors are required to provide a completed W-8BEN Form prior to or at the time they submit their 1st invoice to [accounting@doublegdp.com ](mailto:accounting@doublegdp.com)

Payments are usually processed in Bill.com via the 'International Payment' or via Bank wire payments. 

**Contracted Non-US Team Members**

For contracted non-US team members, a completed W-8BEN Form is collected during the Pilot onboarding process. 

Payments related to the contracted services are usually processed in Pilot monthly. 

Expense reports from the non-US team members are required to submit in Expensify.  

Please refer to the Expense Reports and Pilot sections below for more details.
 

## Expense Reports and Receipts

For expense reports, receipts should be submitted either through **Expensify** or **Brex**, depending on what process was used for payment. 

Please submit all receipts by the 7th of the month as Accounting has the deadline to close the books monthly. For example, the expense report and the receipts of October 2020 need to be submitted by November 7th, 2020

**Business Expenses Paid by your Personal Card**


If you paid business expenses with your personal card, submit your receipts through Expensify. And submit your Expensify Report for approval within the system. 

You can do this through the app, or by forwarding the receipt to [receipts@expensify.com](mailto:receipts@expensify.com) from your DGDP email address.

The reimbursement for US based team members will be processed in Bill.com, unless otherwise advised.

The reimbursement for non-US team members will be processed in Pilot, unless otherwise advised.

**Business Expenses paid by DGDP's Prepaid Card**

If you paid business expenses with the company's pre-paid card, please submit your receipts through Expensify. 

Very importantly, please name the report with the wording such as " Receipts Submission Only". 
For example, "October 2020 Expenses_ Receipts Submission Only". Including these specific wording will be very helpful for Accounting to differentiate the expense reports that do not require reimbursements vesus the ones that they do.


**Business Expenses paid by DGDP's Brex Card**

If you paid business expenses with the company's Brex card, please forward the receipts directly to [receipts@brex.com](mailto:receipts@brex.com).

A few more requests about receipts:

* Make sure to submit an itemized receipt, not the credit card payment
* For business meals or client meetings, please list the business purpose and the names of all the participant in the receipt
* It is recommended that the most senior level colleague must pay for the expense 

**Expense Report Approval**

* Expensify approval process is automated
* Once you submit your expense report, the system will route your expense report to your direct manager for approval 
* After the manager approves in Expensify, Accounting will validate the receipts for accuracy and process payment within 7 days through one of the following methods:

    * Bill.com for US-based team members
    * Pilot for non-US team members
    * Your hiring agency, for non-US team members who are working with DGDP via an agency


**Business Meals and Entertainment Guidelines**

It is important to have team building events. For those of us who have others in our area getting together to visit or celebrate a milestone can form important social connections. DGDP supports the costs of these events periodically. 

The most important thing when spending is that you act reasonable with the company's money. It's hard to give strict guidance on specific prices because what's reasonable will vary from country to country and city to city. You're responsible to interpret those prices relative to your location. That said, here are some guidelines for your consideration:

* Choose moderately priced places for general team meetings or visits. For bigger occasions, pick a place that is appropriate for the cost. 
* For restaurants, this probably means $$ or $$$ on [TripAdvisor](https://www.tripadvisor.com/ShowTopic-g1-i12105-k12250210-Dollar_Sign-Tripadvisor_Support.html) but not $$$$.
* It's okay to spend on drinks, but remember that you are in a professional context and act accordingly. There can be a huge range in price on wine and liquor; stay at or below the mid-range of cost.

## Pilot 

DGDP uses [Pilot](https://pilot.co) to process payments for non-US team members.

When a team member successfully completes the onboarding process, the direct manager of the new hire should notify Accounting immediately via sending an email to [accounting@doublegdp.com](mailto:accounting@doublegdp.com) with the information below:

* Full name of the new hire
* Official start date
* Annual or monthly compensation in USD$

With the new hire's information, Accounting will help to pre-generate the monthly invoices for the next 12 months on behalf of the new hire. If there are any exceptions on this, please notify Accounting by sending an email to [accounting@doublegdp.com](mailto:accounting@doublegdp.com).

Since the team members in Pilot are mostly based outside of the US and the processing time of non-US payment varies, Accounting has developed a monthly payment calendar to streamline the payment schedule and to allow the funds to arrive on or before the last day of each month. 

For new hires who joined on or before the 22nd of a given month, Accounting will try to include the new hire in the monthly payment schedule. If a new hire joined after the 22nd of a given month, Accounting may need to run an off-cycle payment for the new hire. Accounting will notify the new hire when the payment will arrive in the situation of an off-cycle payment.

If you have any questions or concerns about the status of your payment, please contact [accounting@doublegdp.com](mailto:accounting@doublegdp.com).


## Payroll

Payroll for US based full time employees is managed via Gusto. 

DoubleGDP runs semi-monthly payroll schedule. 

Employees will complete the onboarding process through Gusto and set up direct deposit for their payroll payments.  

## Wire Payments

All wires from SVB are initiated by Accounting and approved by DGDP authorized SVB signer(s) per the authorization that is set up with SVB.

These wires include payments to any vendor who cannot get paid via Bill.com or to provides funds to non-US team members for business expenses.

To request a wire payment, please send an email request to Accounting (accounting@doublegdp.com) along with (1) the invoice (2) when the payment is requested to arrive (3) vendor's wire instructions (4) if a foreign currency wire payment is required, please indicate which currency.

Typically, a wire instruction needs to include below:

1. Recipient's name 
2. Recipient's address 
3. Recipient's bank name 
4. Recipient's bank address 
5. Recipient's bank ABA Routing Numbers (if domestic wire within United States)
6. Recipient's IBAN, BIC or SWIFT Code (if international bank outside of United States)

Sometimes bank may require to provide additional information, Accounting will reach out to the wire requestor if such request arise.

Wire payment is usually quicker, but for international wire payment, it still takes a few business days to clear. We suggest you to submit the wire request at lease 7 business days prior to the payment due date to allow accounting to get the wire initiated and approved. 

If the wire payment is urgent, please flag " URGENT Wire Payment" in the subject line when you send the wire request to Accounting.


## Fixed Assets Tracking

Fixed assets such as office furniture, computer equipment, laptops, are purchased for long-term use. They usually have a useful life of more than one year. DoubleGDP tracks its fixed assets via this document [Employee Equipment - Fixed Asset Tracking](https://docs.google.com/spreadsheets/d/1QCesUFmXBiuTcgf2UI5jnc7M4QjqwpcoMhw8rXaGgGs/edit?ts=5fab0d81#gid=0). 

Purchaser of the assets are required to log in to this file to fill out the information (as indicated in the file), such as date of purchase, purchaser's name, value of the asset, etc. 

Any team members received the company purchased laptops or computer equipment are required to log in to this file and fill out the asset description, model #, and serial #. 

Please make sure the information of the Fixed Asset Tracking file is completed within 7 business days of the purchase date when the asset is initially purchased. 

If a transition happens of an existing computer equipment to another team member, the recipient of the computer equipment is required to acknowledge the receipt of the computer equipment within 7 business days. Please log in to the Fixed Asset Tracking file and fill out the required information (as indicated in the file), such as description of the asset transition, date of transition, old owner, new owner, etc.


