## GitLab

We use GitLab for many important communications. Processes leveraging it will be documented on their own pages. This page outlines the setup of GitLab itself.

We have four repositories:

1. **Handbook** - the source code for our Handbook. The audience of the Handbook is DoubleGDP Teammates. This is written for us and by us. This covers company processes. Note that issues created in this project are by default public, and can be marked confidential.
1. **CS Playbook** - our playbook of CS issues and city programs. The audience of the CS Playbook are city developers.  This covers processes related to specific partners or city practices. The playbook itself is public, but issues in this repository are by default private, so that while we create issues we are free to discuss challenges specific to an individual partner or user that they may not want shared publicly.
1. Platform - our product code. Issues and processes in this repo are by default public, as is our source code.
1. Web - our website. 
1. Coding Challenge - to help evaluate engineerng candidates
1. PagerDuty Notifier - to convert SMSs to PagerDuty incidents
1. Certbot - to manage SSL authentication

## Icons
These icons are configured at the top of the projects, and on the Slack incoming webhooks.

- Handbook <img src="/img/icons/dgdp_handbook.png" width="50px" alt="Handbook" />
- CS Playbook <img alt="CSM Playbook" src="/img/icons/city_sun.png" width="50px" />
- Platform <img alt="Platform" src="/img/icons/platform_icon.jpg" width="50px" />
- Website <img alt="Website" src="/img/icons/dgdp_favicon.png" width="50px" />

## GitLab Practices

1. Work that needs to be done and requires collaboration between more than 2 teammates should be created as an `issue` in GitLab. See above for guidance on which Project it should be placed in.
1. Issues may have `todo` items within them. If ambiguous, they should include who owns them and a due date. The "due date" for an individual task is written as text next to the task, not the overall due date for the Issue. Todos may be split into separate issues if they have many subdependencies, if the owner of a sub-dependancy needs to manage the tasks Due Date via reminder in GitLab, or require a separate group of more than 2 teammates to coordinate independently of the main issue.


