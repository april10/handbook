
# People Operations



## Team Meetings

Guidelines for meetings



*   Start on time
*   Use “speedy meetings” from Google -- end 5 or 10 minutes before the hour
*   Record and share when it’s not objectionable and data that clients
*   We have a [rolling agenda](https://docs.google.com/document/d/1vmCANTcdXibSFUF8EW1eh41RlGpvQB8RZ5VOnQLboMA/edit) for team meetings
*   Use [this format](https://about.gitlab.com/handbook/leadership/1-1/suggested-agenda-format/) for note taking


## Meeting Cadence

While sprints are our primary product development process, we also have regular company and other meeting cadences that help us stay coordinated. This sections shows the 2-week meeting cadence alongside coordinated company milestones and Nolan’s 1:1 schedule.

Sprints are 14 calendar days starting on Thursdays to the following Wednesdays at noon Pacific.



<table>
  <tr>
   <td><strong>Day</strong>
   </td>
   <td><strong>Sprint</strong>
   </td>
   <td><strong>Company</strong>
   </td>
   <td><strong>Nolan 1:1s w/</strong>
   </td>
  </tr>
  <tr>
   <td><strong>Thu</strong>
   </td>
   <td><strong><a href="https://docs.google.com/document/d/1OdJ6WJEy5LyR-dZtOtO5elxdYswYqlMX2JaNKG4JcBk/edit">Wrap / Kickoff Sprint Meeting</a> </strong>
<p>
[90 mins]
<p>
<em>Previous sprint retrospective </em>
<p>
<em>Next sprint plan part 1</em>
   </td>
   <td><strong>Investor Call</strong>
   </td>
   <td>Jay
   </td>
  </tr>
  <tr>
   <td>Fri
   </td>
   <td><strong>Planning meeting </strong>
<p>
[45 mins]
<p>
<em>Share feedback from investor call</em>
<p>
<em>Next sprint plan part 2</em>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Sat & Sun
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Mon
   </td>
   <td><em>Development work...</em>
   </td>
   <td><em>Reports due...</em>
   </td>
   <td>Olivier
   </td>
  </tr>
  <tr>
   <td>Tue
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>Mutale
   </td>
  </tr>
  <tr>
   <td>Wed
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Thu
   </td>
   <td><strong>Look-ahead meeting </strong>
<p>
[60 minutes]
   </td>
   <td>
   </td>
   <td>Jay
   </td>
  </tr>
  <tr>
   <td>Fri
   </td>
   <td>
   </td>
   <td><strong><a href="https://jeanduplessis.gitlab.io/breakout-question/#/">Team visit</a> </strong>
<p>
(<a href="https://icebreaker.range.co/difficulty">Icebreaker</a>)
<p>
[45 minutes]
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Sat & Sun
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Mon
   </td>
   <td>
   </td>
   <td><em>Reports due...</em>
   </td>
   <td>Olivier, Mutale
   </td>
  </tr>
  <tr>
   <td>Tue
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>Mark
   </td>
  </tr>
  <tr>
   <td>Wed
   </td>
   <td>Wrap sprint videos due @ noon PT
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
</table>


Overview of the meetings:



1. **Planning** - prioritize tasks to be included in the current sprint
2. **Look ahead meeting (Backlog Refinement)** - review feature suggestions to propose items for next sprint, and discuss key customer insight work facilitated by Jay
3. **Wrap sprint (Sprint Demo) ** - record video updates of progress. Suggest: demo completed works, share highlights and lowlights, discuss what you will tackle next sprint.
4. **Retrospective** - discuss what went well, what could have been better, what would you like to do differently in the next sprint or two?
5. **Investor call** - review key metrics, share highlights and lowlights, discuss next sprint priorities, ask for any assistance needed.

Team Visit Prompts



1. GitLab breakout question: [https://jeanduplessis.gitlab.io/breakout-question/#/](https://jeanduplessis.gitlab.io/breakout-question/#/)
2. Range.co icebreakers: [https://icebreaker.range.co/](https://icebreaker.range.co/)
