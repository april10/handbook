---
title: Communications
---

## Primary Tools

Here are the primary tools that we use to collaborate with each other.

### Slack

Conventions for using Slack:

*   Post to a public channel whenever possible
    *   Posts will be considered FYI unless they mention a specific person
    *   @mention individuals to flag specifically for them
    *   Emoji acknowledgements are appreciated. See more on [GitLab’s page](https://about.gitlab.com/company/culture/all-remote/informal-communication/#using-emojis-to-convey-emotion).
    *   Use @channel for announcements
* Slack channels, even private ones, should **never** be used to discuss specific personnel issues, concerns, or performance.  Membership in Slack channels changes over time and new members also see history; messages posted in channels therefore may at some point be visible to more people than who are included in the channel when it is written. (Personnel issues should properly be addressed in Slack DMs, email, Google Docs that are labeled with `[restricted]` and shared only with specific people, or in our Applicant Tracking System for candidates.)
*   When responding in a public channel start a thread using the dialog above and to the right of the initial message. This will keep the conversation available for all to see but make fewer “unread” messages for teammates that are logging in after not checking Slack for a few days. Look for this icon:

<img alt="slack thread" src="../../img/slack_thread.png" width=200px />


### GitLab

This is our main project management tool. 

* Set To-Dos for each teammate.
* We use epics and issues.
* What are epics?
   * Epics contain a title and a description, and allow teammates to attach issues, which are equivalent to sub-tasks, within the same epic. At DoubleGDP, each client or project would typically have its own epic, and teammates are able to create issues within these epics, as well as assign the issues to different other teammates.
   * Teammates are to use epics to manage their workflow, as it allows us to work iteratively. Epics help us rack status, and delivery of projects.
   *  Different epics for different clients.


### Google Suite

* Docs, Drive, Calendar, Spreadsheets

### Zoom

* For videoconferencing


## Communication Norms

Remote communication can be difficult at times, especially spanning timezones, cultures, work-styles, a variety of applications, and sometimes challenging connectivity issues. The following best practices are adapted from GitLab’s [communication guidelines](https://about.gitlab.com/handbook/communication/) and meant to help us maximize our asynchronous productivity. Some are general and some application-specific.


<table>
  <tr>
   <td>General
   </td>
   <td>
<ol>

<li>Always acknowledge receipt of messages sent directly to you or in which you are mentioned. In email, send a “thank you” or “okay” or “got it;” in calendar respond accept or reject (or even maybe) to invitations; in Slack or GitLab a thumbs up. Acknowledging lets the sender know the message has been received and reduces the risk of “oh I was unaware.” It also alerts the sender that if they don’t receive a response it might have been overlooked and they should check in with you.

<li>Favor written over verbal communication for decisions. If you work through something verbally, write it down so it can be shared with others.

<li>Share “bad news” quickly and straightforwardly. Be thankful to receive it -- bad news is so much harder to share than good news. We’re working on really ambitious and challenging projects; things will go wrong, and sharing them quickly and openly can usually provide good fodder for improvement for the whole company if handled with respect.

<li>Dates should be written in ISO format: yyyy-mm-dd. This is unambiguous, machine readable, and sortable.

<li>Timezone should always be specified. We operate in many time zones (e.g. PT, CT, WAT, CAT, EAT, HKT, IST), so this is important to avoid ambiguity.

<li>Use “DoubleGDP” except in urls and email addresses, where it’s “doublegdp”.  
<ol>

<li>Note that FB & WA & Instagram require us to use “Double GDP” in our name, so you’ll see it written incorrectly there.
</li>
</ol>
</li>
<li>In order to not miss a notification or meeting, you can download the Slack and Google Calander mobile apps. It is not a requirement, but a nice to have.</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>Slack
   </td>
   <td>
<ol>

<li>Use public channels whenever possible. Threads within public channels can prevent spamming people, while still allowing others to be aware of what’s going on and to contribute if they have something to add.
<ol>

<li>Note that GitLab has a goal to have 80% of communication in public channels. (As of 2020-02-26, we’re at 35%)
</li>
</ol>
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>Drive
   </td>
   <td>
<ol>

<li>Documents by default are visible to everyone in the company who has the link. You may disable this if you want to keep a document private

<li>If you do share the document with anyone, generally share it with <a href="mailto:all@doublegdp.com">all@doublegdp.com</a>, but communicate directly with any individuals from whom you need input.  
<ol>

<li>This allows everyone to contribute and saves a lot of time when one stakeholder decides to add another, who may then need to request access from someone who has already stopped work for the day.

<li>Conversely, documents shared to all@ should be considered “FYI” unless someone follows up directly with you. There’s no obligation to read every document that’s shared.
</li>
</ol>
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>YouTube
   </td>
   <td>We post our sprint meetings on YouTube. We value transparency about what we’re working on and you’re welcome to share strengths and weaknesses, works in progress, highlights or concerns. However, we must also respect that not every external partner will share this level of comfort. Therefore we must be sure to avoid any of the following, in both our verbal narrative and any screens we share in presentations:
<ol>

<li>Personally identifying information who are not DoubleGDP team members. E.g. no mention of names, license plates, phone numbers, emails.

<li>Statistics or metrics of our partners’ data
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>Docs
   </td>
   <td>
<ol>

<li>Use numbered lists instead of bullets. They allow for being able to reference easily in conversation.
</li>
</ol>
   </td>
  </tr>
</table>


### Not Public & Limited Access

Our default is transparency, but there is sensitive information that can either not be shared publicly or can only be shared with a limited internal group. We follow Gitlab's guidelines for [not public](https://about.gitlab.com/handbook/communication/#not-public) and [limited access](https://about.gitlab.com/handbook/communication/#limited-access) information. 

Of particular note, no information containing Personal Identifying Information 'PII' of colleagues or customers should be shared externally. 
