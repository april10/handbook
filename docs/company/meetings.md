---
title: Meeting Norms
---

Meetings are an important aspect of effective collaboration. They are also an expensive investment of teammate time, and (especially importantly) the synchronous time that can be scarce when working across disparate time zones. The following norms aim to establish efficient and effective practies.

## Meeting Norms

### Scheduling meetings

To schedule Zoom meetings from within Google Calendar:

1. Open Google Calendar, tap the plus icon then Event.
2. Enter the meeting details, add invitees, or location.
3. Tap Add conferencing and select Zoom Meeting. Google Calendar will add a Zoom Meeting to your meeting details.
4. Tap Save.

### Meeting format

1. Meetings must be scheduled using [Google Calendar](https://calendar.google.com) and should have a zoom link included
1. All meetings must have an agenda in a shared Google Doc that's lnked in the Description. Items for discussion must be listed in the agenda prior to the start of the meeting.
1. Meetings start right on time, and must be "[speedy meetings](https://gmail.googleblog.com/2011/06/change-google-calendars-default-meeting.html)." This means they end 5 minutes prior to a 30-minute slot or 10 minutes prior to an hour slot
1. All teammates should have the meeting notes open during the meeting and add questions or comments in the doc in real time. Done well, this is much more effective than using Zoom chats.
1. The bi-weekly team meeting is a ritual that focuses on community-building not efficiency, and operates under a different approach to setting and managing agendas.


### Note taking during meetings

1. Everyone has the agenda doc open. Agendas can be "rolling" or "by date"
1. Discussion flows linearly through the doc -- from top down if organized "by date" and from bottom up if considered "rolling"
1. Each teammate adds their point or question in real time during the call. Always add your point below others' that that the linear flow is maintained
1. Use numbers instead of bullets so they are more easily referenced in conversation
1. Each person's point should be its own bullet, so that you don't type on the same line as a teammate

## All Team + Investor Call
We periodically have an opportunity for the entire team to meet with our investor for a Q&A. (These are scheduled roughly monthly.) They are intended as an opportunity for the team to ask questions directly of our investor.

Here are "norms" for the call:

* Prior to the call, please add a question or two to the meeting agenda. Prefix the question with your name.
* In the call, we will go through them in order, giving each person the chance to introduce themself and voice their question.
* There's a set of "bench questions" to help spur your thinking, or for us to use to help start a conversation -- feel free to grab one and make it your own. (Team questions take priority over "Nolan" questions, since he has the chance to talk more frequently.)
* Previous questions are captured in notes below, and are also fair game to repeat if you're new or the timing may change the context. Just re-phrase it and make it your own by adding it to the top of the agenda.
* Note that this call is joined by GitLab's "CEO Shadows" -- please see more about this program here: [https://about.gitlab.com/handbook/ceo/shadow/](https://about.gitlab.com/handbook/ceo/shadow/)


## New Teammate Welcome Call

When a new teammate joins, we take the opportunity for the whole company to come together and welcome them to the team. Like the All Team call, this call is different than all other DoubleGDP meetings because it is mostly focused on the team  we're building, not our product or programs or customer relationships. We do this ritual to celebrate the growth of our team, to reflect back on how we've grown over time and how every single new person has changed the trajectory of our company, to form new relationships among ourselves and to help everyone know the newest member of the DoubleGDP team.


* We go through in order of Start Date from our ![Team Milestones](https://docs.google.com/spreadsheets/d/1POhDzesk5fEjiafkWxJmsFnjbY5F_c--0fmFdSiO2vE/edit#gid=0)
* Each person introduces themselves, explains their role, and shares some tidbit about themselves (which changes for each meeting).
* Each person then calls on the next teammate on the list.
