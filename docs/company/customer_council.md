## Customer Council

Every quarter we hold a customer council meeting. The purpose of this meeting is to ensure that we are focused on the solving the problems that are most important and will create the highest value for our customers. Here is our process:

1. 5 days before the Customer Council Meeting, Nolan sends a 7-min or less video that is **Company and Product Update** to all meeting attendees for them to pre-watch before the meeting.
1. Nolan includes a request for responses to the video and 1-2 questions. One question should always be “what issues/questions do you want to discuss?”
1. Customer Council members respond via email and send questions that they'd like us to discuss.
1. Two days before the meeting, Strategy Team meets to prepare talking points, choose a focus area, set roles and define any updates from the video.
1. In the meeting, we spend 5 minutes updating from what we heard. Optional: include 1-slide summarizing those updates.
1. We have the meeting.
1. Two weeks after the meeting, we schedule the next quarter's meeting and get on everyone's calendar.


Here is a draft set of objectives and agenda that can be used to prepare:

**Objectives**

1. Check our hypotheses about most important priorities
1. Review DoubleGDP progress and near-term plans
1. Discuss opportunities, concerns and roadblocks
1. Align on next steps


**Agenda**

- Welcome and Objectives (5 min)
- Q&A From Product Review (video sent prior to meeting) (10 min)
- Focus Area (15 min)
- Most Important Needs & Implications for Product (15 min)
- Next Steps: "To recap, what one thing we could do in the next 3 months that would be most helpful?" (5 min)


**Most Recent Meeting**
Here are the slides from our most recent [Council Meeting](https://docs.google.com/presentation/d/13RsZG4xL65lheB9Gkr31bThD10nyotWw8v0V2TT2Fb4/edit#slide=id.g5673e8a826_0_5)
