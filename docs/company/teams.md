---
title: Teams
---

# Teams at DoubleGDP

Each team at DoubleGDP has a _charter_ with high level responsibilities.

* **Charter** - _“a written constitution or description of an organization's functions.”_

### Engineering
Responsible for architecting, building, and maintaining product features.

1. Recruit and build a “world-class”, fast-moving team.
1. Invest in engineers skills through mentorship and/or training.
1. Collaborate with product to develop valuable, small, and independent stories
1. Build iteratively scalable software architecture that meets the product requirements.
1. Release stable Product features
1. Achieve consistent feature delivery
1. Automate builds and deployment
1. Responsible for production support and uptime.
1. Manage the engineering Roadmap
1. Protect data and systems entrusted to us

### Customer Success
Drive adoption of the product among customers and users

1. Ensure awareness among users of what the product can do and the value it can deliver to them
1. Identify unmet needs and ensure Product Management is aware of them
1. Answer client questions and provide support and sales
1. Run operations & logistics necessary to keep systems and data working on site
1. Assist Security with upto date knowledge on systems

Within customer success, we have two focus areas: operations and content marketing.

#### Content Marketing
*Accelerate growth through engaging, informative, and promotional content.*

1. Define relevant content for each audience
1. Project manage content development and publication
1. Ensure engagement and connection with audiences on all social media platforms
1. Source content ideas from team, admins, clients
1. Monitor and report success of marketing campaigns
1. Document best practices to help customers build their own content
1. Propose and advocate product enhancements that improve our content marketing capabilities

#### Operations
*Connect with the city community and ensure responsive services*

1. Help users to understand and make good use of the application
1. Monitor and improve user satisfaction - both community members and admin teams
1. Ensure responsiveness of support communications
1. Build and support processes for operational teams (e.g. guards, contractors, sales team, finance team)
1. Demonstrate and document best practices so to help customers run efficient operations leveraging DGDP
1. Propose and advocate product enhancements that improve responsiveness and satisfaction

### Product Management
Responsible for creating products and experiences that customers and users love and value

1. Identify valuable use cases for our customers (Nkwashi now) and their stakeholders
1. Articulate customer and stakeholder needs into manageable stories for engineering development
1. Measure and iterate product based on adoption and feedback

### Product Design
Responsible for improving the user experience and the interface design of DoubleGDP's platform

1. Plan and implement new designs, optimise existing user interfaces, and improve the intuitivity and experience of the platform
1. Support the development of requirements to deliver user-centered solutions
1. Test new product ideas through prototypes and user research
1. Provide support for partnerships and marketing material and collateral
1. Collaborate closely with customers, product, and development teams, and effectively communicate design decisions across all stakeholders



### Partnerships
Responsible to bring in new partners who will utilize our platform (as it is or as we can envision it) for their users

1. Assess broader market fit for our product and our vision (“Prove that there’s a wider market”)
1. Find new partners who align with our values
1. Pursue a “long nurture” approach in building relationships with partners
1. Help, don’t sell
1. Understand partner's goals and where we can contribute
1. Inspire partners to enter a “testing agreement” like the one we have with Nkwashi

"Partners" include cities / property developers / community organizations, and their users include staff, residents, businesses, and prospective members of their communities.

### CEO
Responsible for forming strategy, with the team and values to achieve it.

1. Help team focus on what we need to succeed: make our customers successful, iterate with the smallest step each time; follow a "handbook-first" approach to process improvement
1. Support team in achieving their charters; help build alignment among them
1. Handle functions of company not directly in another team’s charter — e.g. people operations, legal, misc.

### Investors/Owners

Responsible to invest money and provide advice.


## Organizational Structure and Accountabilities

Here is our organizational structure, as it relates to personnel management:

-  Nolan (CEO)
    - Nicolas (Head of Engineering)
        - Olivier, Saurabh, Dennis, Tolu, Nurudeen, Victor -- (Engineers)
    - Minza (Product Manager) 
        - Gbemisola (Design)
    - Jay (Head of Partnership)
        - Justin (Strategic Partnerships Manager)
    - Doreen (Head of Customer Success) 
        - Penny, Mutale, Zoussi (Customer Success Managers)
    - April - (Operations Manager)
    - David (CFO) [contract] via [Formation Financial](https://www.formationfinancial.com/)]
        - Jie (Controller), Harry (Accountant)
    - Vish (Legal) [contract]
    - Matt (Content Marketing) [contract]

Our key processes and projects are managed as follows:

- Overall company operating performance (Nolan)
    - Nkwashi product and success (Minza)
        - Plot marketing and sales (Penny)
        - Community engagement and satisfaction (Mutale)
        - AIR (Zoussi, transitioning from April)
    - Zone Hub product and success (Nolan)
        - Customer Succes (Zoussi)
    - New Partners (Jay)
    - Recruiting (Ren)
    - Finance & Payroll (David)
    - Process improvement (All Managers)

### Operations Management

The purpose of the Operations Manager role is to create efficient and effective asynchronous company processes. This includes the following:

* Document, champion and improve handbook-first process at DGDP
* Onboard and offboard team members with company values and culture
* Adhoc assignments and special projects from CEO
* Migrate tasks and projects to GitLab from Asana
* Actively engage with teammates for feedback on new workflow in GitLab
* Collaborate with DGDP team to solicit feedback 

To reach out to April, @mention her in the #peopleops channel of Slack, or assign a GitLab issue to her directly. (If you have questions that you want to remain confidential, email is best.)









