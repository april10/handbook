# Metrics


## Weekly Active Users

Increasing WAUs is the primary measure of company performance at this point, and tracks to an “aspirational target” of 10% week over week growth. Assuming a baseline of 15 WAUs from 2019-11-12, the week we initially deployed to Nkwashi, that growth curve would predict 4567 WAUs by the end of 2020, and so we have had discussions about a target of 5000 WAUs as we think about our company planning. We should use this to inspire us to think big about what we can do and how we should design our product. However, we have not formalized this into a “goal.”


### WAU Measurement 

1. We measure weekly active users (WAUs) via the "All Users" line in [Google Analytics](https://analytics.google.com/analytics/web/?authuser=1#/report/visitors-actives/a150647211w230363140p216746025/). We look at the 7-day actives, and use the Monday-Sunday period as our external reporting line.
1. Also of note is the "Logged In Users", which can be defined as a segment. See instructions below.
1. Each week we take those two metrics from Google Analytics (GA) and record them in our [WAU Report](https://docs.google.com/spreadsheets/d/1AgLRuZmKEQVS7qaFcEqyUvDvUIgHAbwFNQfhb491YTs/edit#gid=88261873), which also has a variety of other data.
    1. The other data fields on WAU need updating from within the spreadsheet. Those steps are listed below.


Manual steps to generate the report:

1. Copy and Paste the "All Users" and "Logged In Users" from Google Analytics data from Sunday into the proper week in the [GA Analysis](https://docs.google.com/spreadsheets/d/1AgLRuZmKEQVS7qaFcEqyUvDvUIgHAbwFNQfhb491YTs/edit#gid=88261873) tab
2. Update each of the tabs with a database symbol by pressing the `Refresh` button. (These are named `WAU_Data`, `Feedback_Data`, `Gate_Data`, etc.)
1. Look at the new totals on the `Weekly Progress` tab

#### Logged In User Segment
Here's how to define the Logged In User segment in Google Analytics:
![](/docs/img/logged_in_user_segment.png)

### Process when we miss goal
We aim to hit the target week over week. However, growth is not always continuous and there are times when we won't hit the goal. When this happens, it's a good opportunity for us to understand what went wrong (if applicable) and refocus our energy and creative spirit to get back on track. Our process when this happens is as follows:

1. Each teammate takes 10-15 minutes to [brainstorm 5-10 ideas](https://forms.gle/4Tpd8PedNHYZXxtH8) to increase WAU. 
1. Flag ideas that you particularly like with `**`
1. Nolan will review all ideas and choose a few to focus on
1. In the next Sprint Retrospective, we will brainstorm what would be some iterative approaches to those ideas, focusing on simple and actionable near-term steps. For example: can we get 50% of the value of the idea at 10% of the cost? What's the smallest iteration that can make some progress toward the goal?
1. We'll write a few stories to add to the sprint about to kick off

