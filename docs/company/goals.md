# Company Goals

Our north star goal is to hit 5000 weekly active users by the end of 2020. We haven't set a formal target for next year yet, but take our inspiration from Paul Graham's article on [growth for startups](http://www.paulgraham.com/growth.html). 


Within this north star, we have a few sub-goals to help define accountabilities for individual processes or projects. These are listed in 

1. **Minza** to play point on all things Nkwashi. Key initiatives within the account. Grow to 4000 WAUs.
    1. Unified marketing and sales campaign around 99-year-lease plots (**Penny**) (4000 WAUs driving towards the goal of 5000 with 1000 coming from Zonehub. Continual shift in marketing content to target and convert more eligible prospective clients by leveraging the 99-year-lease initiative)
    1. Drive adoption among existing Nkwashi community. User sat, responsiveness, and drive toward “construction starts” (**Mutale**) (400 WAUs)
    1. Producers In Residence program[^pir] to create economic activity and agglomeration (**Zoussi**, with **April’s** support) (1000 WAUs)
1. **Nolan** to run point on Zone Hub, with Zoussi driving. Grow to 1000 WAUs
1. **Minza** to continue to drive product roadmap for anything not directly under the Zone Hub epic (1 value added feature inspired by an end user per sprint)
1. **Jay** to bring in new city test partners, prioritizing large new city property developers in Africa. (1 new partner by 2020-12-31)
1. **Nicolas** to build high-velocity engineering team -- (Grow to team of 8 engneers. Deliver 3 MRs per Eng per sprint. 80% code coverage)
1. **Zoussi** to lead the Artists in Residence Program at Nkwashi, with the goal of converting interest generated from the program into people moving into Nkwashi City.
1. **Gbemi** to lead the design of product to deliver a better experience for our clients ( 1 improved design per feature in each sprint)
1. **All teammates** -- incrementally improve company processes -- (1 merge request to the handbook per teammate per sprint)

### Goals Process

We have company goals to help each person understand what results the company is relying on them to deliver. Thus, each goal aims to have a Directly Responsible Individual (DRI), an Objective, and a Key Result (KR) that represents how we will evaluate its success. This format is inspired by "[OKRS](https://felipecastro.com/en/okr/what-is-okr/)", but expressed as simply as possible in its current forms. We will reassess goals at least quarterly, and more frequently if our business or operating context requires it.

Note that teams and individuals are welcome to develop more specific subgoals to the extent that they will help organize their effort or coordinate with others.   


[^pir]: "Producers In Residence" is the umbrella term for results from Artists in Residence and Hackers in Residence
