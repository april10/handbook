
## Common terms

Here are some terms we use at DoubleGDP that may require definition

Client 
:   A paying customer of our `customer`, i.e. a person who is buying or leasing a plot of land. 

Customer
:   Also referred to as "Partner" -- a city administrative team who is leveraging the DoubleGDP platform to connect with their residents

Nkwashi App
:   The DoubleGDP Platform, as viewed by someone within the Nkwashi community 

Partner
:   Also sometimes referred to as "Customer". See that entry for definition. We often use "partner" in the context of our sales pipeline to convey that we are seeking new cities who want to partner with us on the journey of building great software and cities rather than those who are stricly looking for a software platform.

Prospect

:   Generally refers to a *prospective client* of our customer, i.e. a person who is considering buying a plot of land in the city
:   It may also refer to a *prospective partner* of ours, i.e. a city administrative team who we are discussing partnering with to use our software

User
:   One who is using the DoubleGDP platform, who could be a client or prospect of our customer, or even someone on the administrative team


