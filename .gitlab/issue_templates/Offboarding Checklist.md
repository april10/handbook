Here are the steps required to offboard employees:

## Manager TODOs
**Administrative**
- [ ] Delete the employees "hello world" in the handbook (https://handbook.doublegdp.com/handbook/)
- [ ] Remove picture and bio from [our website](https://www.doublegdp.com/about/).
- [ ] Mark as inactive on the [company milestones](https://docs.google.com/spreadsheets/d/1POhDzesk5fEjiafkWxJmsFnjbY5F_c--0fmFdSiO2vE/edit#gid=0)
- [ ] Remove employee and role from [Handbook Teams Page](https://gitlab.com/-/ide/project/doublegdp/handbook/tree/master/-/docs/company/teams.md/)


## System Administrator TODOs
For the system administrator to complete. (Manager: please assign to either Nolan or April.)
- [ ] Remove from Google Suite, and any group membership within (e.g. "eng" or "csm")
- [ ] Remove from GitLab - use "Maintainer" role under the "DoubleGDP" Group by default
- [ ] Remove Slack access
- [ ] [Google Analytics](https://analytics.google.com) (should be automatic via SSO)
- [ ] Remove from Pilot
- [ ] Remove from DGDP[YouTube](https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig)
- [ ] Remove from [OnePassword](https://doublegdp.1password.com/)
    - [ ] Double check individual vault access
- [ ] Remove [Zoom](https://zoom.us) (if appliable) - use "Basic" account for engineers, "Licensed" account for teammates who we expect to host meetings frequently
- [ ] Remove from Expensify (if applicable) - only remove if there are no pending transactions/expense reports
- [ ] Remove from [AirTable CRM](https://airtable.com/invite/l?inviteId=invZ8AGEsyzttooGu&inviteToken=9a51ce3c8881f4d03e07707654a7cead06c16e061133db859934848b515f20fe) (if applicable)


### Reference: Additional removal for Engineers
- [ ] Remove from Development Environment
    - [ ] Digital Ocean
    - [ ] Google SSO
    - [ ] Facebook SSO
    - [ ] Certficate SSL
    - [ ] CloudflareDNS
- [ ] Remove from Rollbar

